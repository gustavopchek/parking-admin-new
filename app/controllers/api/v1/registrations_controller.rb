class Api::V1::RegistrationsController < Api::V1::BaseController

  skip_before_action :authenticate!, :only => [:create]

  def create

    # render json: user_params

    if(user_params[:password] != user_params[:password_confirmation])
      return password_different
    end

    customer = Customer.new(user_params)

    if customer && customer.save
      sign_in :customer, customer
      render(
        json: Api::V1::SessionSerializer.new(customer, root: false).to_json,
        status: 201
      )
    else
      render(
        json: {errors: customer.errors},
        status: 422
      )
    end
  end

  def update


    # return invalid_user_params unless @current_customer

    # render json: @current_customer

    if(params[:type] == "customer")

      if user_params[:id] != @current_customer.id.to_s
        return unauthorized!
      end

      if !@current_customer.valid_password?(user_params[:current_password])
        @current_customer.errors.add :current_password, "Senha Atual não é válida"
        render(
          json: {errors: @current_customer.errors},
          status: 422
        )
      else

        if(user_params[:password])
          if(user_params[:password] != user_params[:password_confirmation])
            return api_error(status: 401)
          end
        end

        customer = Customer.find(user_params[:id])
        
        if customer && customer.update(user_params)
          render(
            json: Api::V1::CustomerSerializer.new(customer, root: false).to_json,
            status: 200
          )
        else
          render(
            json: {errors: customer.errors},
            status: 422
          )
        end
      end
    elsif(params[:type] == "agent")

      # render json: user_params[:id] != @current_agent.id.to_s
      if user_params[:id] != @current_agent.id.to_s
        return unauthorized!
      end

      if !@current_agent.valid_password?(user_params[:current_password])
        @current_agent.errors.add :current_password, "Senha Atual não é válida"
        render(
          json: {errors: @current_agent.errors},
          status: 422
        )
      else

        if(user_params[:password])
          if(user_params[:password] != user_params[:password_confirmation])
            return api_error(status: 401)
          end
        end

        agent = Agent.find(user_params[:id])
        
        if agent && agent.update(user_params)
          render(
            json: Api::V1::AgentSerializer.new(agent, root: false).to_json,
            status: 200
          )
        else
          render(
            json: {errors: agent.errors},
            status: 422
          )
        end
      end
    else
      unauthorized!
    end

    
  end

  private

  def password_different
    render(
      json: {:errors => {"password":["Os campos de senha estão diferentes."]}},
      status: 422
    )
  end

  def user_params
    params.require(:user).permit(:id, :name, :phone, :cpf, :email, :password, :password_confirmation, :current_password)
  end
end