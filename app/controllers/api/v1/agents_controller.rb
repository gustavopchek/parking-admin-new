class Api::V1::AgentsController < Api::V1::BaseController
  include ActiveHashRelation

  def index
    return unauthorized!
    # agents = Agent.all

    # agents = apply_filters(agents, params)

    # render(
    #   json: ActiveModel::Serializer::CollectionSerializer.new(
    #     agents,
    #     serializer: Api::V1::AgentSerializer,
    #     root: 'agents',
    #   )
    # )
  end
  
  def show

    if params[:id] != @current_agent.id.to_s
      return unauthorized!
    end

    agent = Agent.find(params[:id])


    render(json: Api::V1::AgentSerializer.new(agent).to_json)
  end
end
