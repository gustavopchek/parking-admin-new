class Api::V1::PenaltiesController < Api::V1::BaseController
  include ActiveHashRelation

  def index
    penalties = Penalty.all

    penalties = apply_filters(penalties, params)

    render(
      json: ActiveModel::Serializer::CollectionSerializer.new(
        penalties,
        serializer: Api::V1::PenaltySerializer,
        root: 'penalties',
      )
    )
  end
  
  def show
    penalty = Penalty.find(params[:id])

    render json: Api::V1::PenaltySerializer.new(penalty).to_json
  end

  def customer
    return invalid_user unless @current_customer

    penalties = []

    plates = @current_customer.plates

    # render json: plates

    plates.each do |plate|
      platePenalties = plate.penalties
      platePenalties.each do |platePenalty|
        penalties << platePenalty
      end
    end

    render(
      json: ActiveModel::Serializer::CollectionSerializer.new(
        penalties,
        serializer: Api::V1::PenaltySerializer,
        root: 'penalties',
      )
    )
  end

  def agent
    return invalid_user unless @current_agent

    penalties = @current_agent.penalties.order(id: :desc)

    render(
      json: ActiveModel::Serializer::CollectionSerializer.new(
        penalties,
        serializer: Api::V1::PenaltySerializer,
        root: 'penalties',
      )
    )
  end

  def plate
    penalties = Penalty.where(params[:plate_id]).order(id: :desc)

    render(
      json: ActiveModel::Serializer::CollectionSerializer.new(
        penalties,
        serializer: Api::V1::PenaltySerializer,
        root: 'penalties',
      )
    )
  end

  # POST /penalties
  # POST /penalties.json
  def create

    # Precisa receber e fazer validação de token para verificar se corresponde ao customer_id

    return invalid_user unless @current_agent

    plate = Plate.find_by(code: params[:plate_code])
    return api_error unless plate

    @penalty = Penalty.new(penalty_params)

    @penalty.plate = plate;
    @penalty.agent = @current_agent

    if @penalty.save
      render json: Api::V1::PenaltySerializer.new(@penalty).to_json, status: 200
    else
      render json: @penalty.errors, status: 422
    end
  end

  # POST /penalties
  # POST /penalties.json
  def payment

    # Precisa receber e fazer validação de token para verificar se corresponde ao customer_id

    return invalid_user unless @current_customer

    # render json: params

    penalty = Penalty.find(params[:penalty_id])
    return api_error unless penalty

    # render json: penalty

    @penalty_payment = PenaltyPayment.new()

    if(!penalty.penalty_payment.nil?)
      render json: {errors:["Esta multa já foi paga."]}, status: 422
    elsif(@current_customer.cash < penalty.value)
      render json: {errors:["Saldo insuficiente."]}, status: 422
    elsif(penalty.limit < DateTime.now)
      render json: {errors:["Limite para pagamento expirado."]}, status: 422
    # elsif(@penalty_payment.value.to_s != params[:penalty_id])
      # render json: {payment_value:["Valor do pagamento precisa ser o mesmo valor da multa."]}, status: 422
    else

      @penalty_payment.value = penalty.value
      @penalty_payment.penalty = penalty

      if @penalty_payment.save

        newcash = @current_customer.cash - @penalty_payment.value
        @current_customer.cash = newcash
        @current_customer.save

        # render json: @penalty_payment, status: 200
        render json: Api::V1::PenaltySerializer.new(penalty).to_json, status: 200
      else
        render json: @penalty_payment.errors, status: 422
      end
    end
  end

  def cancelPayment

    @plate = Plate.find(params[:id])

    return invalid_customer unless @current_customer

    if(@current_customer.plates.include?(@plate))
      if(@current_customer.plates.delete(@plate))
        render json: {:flash => ["Placa excluída com sucesso."]}, status: 200
      else
        render json: {errors:{plate:["Não foi possível excluir a placa."]}}, status: 422
      end
    else
      render json: {errors:{plate:["Não foi possível excluir a placa."]}}, status: 422
    end
    
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_penalty
      @penalty = Penalty.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def penalty_params
      params.require(:penalty).permit(:description, :value, :plate_id)
    end
end
