class Api::V1::CustomersController < Api::V1::BaseController
  include ActiveHashRelation

  def index
    return unauthorized!
    # customers = Customer.all

    # customers = apply_filters(customers, params)

    # render(
    #   json: ActiveModel::Serializer::CollectionSerializer.new(
    #     customers,
    #     serializer: Api::V1::CustomerSerializer,
    #     root: 'customers',
    #   )
    # )
  end
  
  def show

    if params[:id] != @current_customer.id.to_s
      return unauthorized!
    end

    customer = Customer.find(params[:id])


    render(json: Api::V1::CustomerSerializer.new(customer).to_json)
  end
end
