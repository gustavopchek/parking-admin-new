class Api::V1::TicketsController < Api::V1::BaseController
  include ActiveHashRelation

  def index
    tickets = Ticket.all

    tickets = apply_filters(tickets, params)

    render(
      json: ActiveModel::Serializer::CollectionSerializer.new(
        tickets,
        serializer: Api::V1::TicketSerializer,
        root: 'tickets',
      )
    )
  end
  
  def show
    ticket = Ticket.find(params[:id])

    render(json: Api::V1::TicketSerializer.new(ticket).to_json)
  end
end
