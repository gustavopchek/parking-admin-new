class Api::V1::ActivationsController < Api::V1::BaseController
  include ActiveHashRelation

  def index
    activations = Activation.paginate(:page => params[:page])

    activations = apply_filters(activations, params)

    render(
      json: ActiveModel::Serializer::CollectionSerializer.new(
        activations,
        serializer: Api::V1::ActivationSerializer,
        root: 'activations',
      )
    )
  end
  
  def show
    activation = Activation.find(params[:id])

    render json: Api::V1::ActivationSerializer.new(activation).to_json
  end

  def last_activation
    activation = @current_customer.activations ? @current_customer.activations.last : null
    # activation = Activation.where(params[:customer_id]).last

    if(activation)
      render json: Api::V1::ActivationSerializer.new(activation).to_json
    else
      render json: []
    end
  end

  # Only Agent
  def last_activations

    return invalid_user unless @current_agent

    plate = Plate.find_by(code: params[:plate_code])

    return api_error unless plate

    activations = plate.activations.order(id: :desc).last(3)

    # render json: activations

    render(
      json: ActiveModel::Serializer::CollectionSerializer.new(
        activations,
        serializer: Api::V1::ActivationSerializer,
        root: 'activations',
      )
    )
  end

  def customer
    activations = @current_customer.activations.order(id: :desc)

    render(
      json: ActiveModel::Serializer::CollectionSerializer.new(
        activations,
        serializer: Api::V1::ActivationSerializer,
        root: 'activations',
      )
    )
  end

  # POST /activations
  # POST /activations.json
  def create

    return invalid_user unless @current_customer

    @activation = Activation.new(activation_params)

    @activation.customer = @current_customer

    ticket = Ticket.find(@activation.ticket_id)
      return invalid_ticket unless ticket

    plate = Plate.find(@activation.plate_id)
      return invalid_plate unless plate

    if(@current_customer.cash < ticket.price)
      return not_enough_cash
    end

    if @activation.save
      newcash = @current_customer.cash - ticket.price

      @current_customer.cash = newcash

      @current_customer.save

      render json: Api::V1::ActivationSerializer.new(@activation).to_json, status: 200
    else
      render json: @ticket.errors, status: 422
    end
  end

  private

    def invalid_ticket
      warden.custom_failure!
      render(
        json: {:errors => ["Ticket não encontrado."]},
        status: 401
      )
    end

    def invalid_plate
      warden.custom_failure!
      render(
        json: {:errors => ["Placa não encontrada."]},
        status: 401
      )
    end

    def not_enough_cash
      warden.custom_failure!
      render(
        json: {:errors => ["Saldo insuficiente."]},
        status: 401
      )
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_activation
      @activation = Activation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def activation_params
      params.require(:activation).permit(:ticket_id, :customer_id, :plate_id)
    end
end
