class Api::V1::PlatesController < Api::V1::BaseController
  include ActiveHashRelation

  def index
    plates = Plate.all

    plates = apply_filters(plates, params)

    render(
      json: ActiveModel::Serializer::CollectionSerializer.new(
        plates,
        serializer: Api::V1::PlateSerializer,
        root: 'plates',
      )
    )
  end

  def customer

    return invalid_customer unless @current_customer

    plates = @current_customer.plates

    render(
      json: ActiveModel::Serializer::CollectionSerializer.new(
        plates,
        serializer: Api::V1::PlateSerializer,
        root: 'plates',
      )
    )
  end
  
  def show
    plate = Plate.find(params[:id])

    render json: Api::V1::PlateSerializer.new(plate).to_json
  end

  # POST /plates
  # POST /plates.json
  def create

    @plate = Plate.new(plate_params)

    return invalid_customer unless @current_customer

    current_plate = Plate.find_by(code: @plate.code)

    # render json: !current_plate

    if !current_plate
      if @plate.save
        current_plate = @plate
      else
        render json: {errors: @plate.errors}, status: 422
      end
    end

    if(current_plate)
      if (current_plate.customers.include?(@current_customer))
        render json: {errors:{plate: ["Esta placa já está cadastrada."]}}, status: 422
      elsif (current_plate.customers << @current_customer)
        render json: Api::V1::PlateSerializer.new(current_plate).to_json, status: 200
      else
        render json: {errors:{plate: ["Não foi possível cadastrar a placa."]}}, status: 422
      end
    end

  end

  # DELETE /admin/plates/1
  # DELETE /admin/plates/1.json
  def destroy

    @plate = Plate.find(params[:id])

    return invalid_customer unless @current_customer

    if(@current_customer.plates.include?(@plate))
      if(@current_customer.plates.delete(@plate))
        render json: {:flash => ["Placa excluída com sucesso."]}, status: 200
      else
        render json: {errors:{plate:["Não foi possível excluir a placa."]}}, status: 422
      end
    else
      render json: {errors:{plate:["Não foi possível excluir a placa."]}}, status: 422
    end
    
  end

  private

    def invalid_customer
      warden.custom_failure!
      render(
        json: {:errors => ["Usuário não encontrado."]},
        status: 401
      )
    end

    def invalid_value
      warden.custom_failure!
      render(
        json: {:errors => ["Valor incorreto."]},
        status: 401
      )
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_plate
      @plate = Plate.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def plate_params
      params.require(:plate).permit(:code)
    end
end
