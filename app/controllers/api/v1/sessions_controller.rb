class Api::V1::SessionsController < Api::V1::BaseController

  skip_before_action :authenticate!

  def create
    # customer = Customer.find_by_email(create_params[:email])
    # customer = Customer.find_by(email: "gustavohpk@yahoo.com")

    # render json: create_params[:type]

    if(create_params[:type] == "customer")
      customer = Customer.find_for_database_authentication(email: create_params[:email])
        return invalid_login_attempt unless customer


      if customer.valid_password?(create_params[:password])
        sign_in :customer, customer
        render(
          json: Api::V1::CustomerSerializer.new(customer, root: false).to_json,
          status: 201
        )
      else
        invalid_login_attempt
      end
    elsif(create_params[:type] == "agent")
      agent = Agent.find_for_database_authentication(email: create_params[:email])
        return invalid_login_attempt unless agent

      if agent.valid_password?(create_params[:password])
        sign_in :agent, agent
        render(
          json: Api::V1::AgentSerializer.new(agent, root: false).to_json,
          status: 201
        )
      else
        invalid_login_attempt
      end
    else
      invalid_login_attempt
    end

    # customer = Customer.find_for_database_authentication(email: create_params[:email])
    #   return invalid_login_attempt unless customer

    # # render(
    # #   json: create_params,
    # #   status: 201
    # # )

    # if customer.valid_password?(create_params[:password])
    #   sign_in :customer, customer
    #   render(
    #     json: Api::V1::SessionSerializer.new(customer, root: false).to_json,
    #     status: 201
    #   )
    # else
    #   invalid_login_attempt
    # end

    # if customer && customer.authenticate(create_params[:password])
    #   self.current_customer = customer
    #   render(
    #     json: Api::V1::SessionSerializer.new(customer, root: false).to_json,
    #     status: 201
    #   )
    # else
    #   render(
    #     json: {:errors => ["Usuario ou senha incorretos."]},
    #     status: 401
    #   )
    # end
  end

  private

  def invalid_login_attempt
    warden.custom_failure!
    render(
      json: {:errors => ["Usuario ou senha incorretos."]},
      status: 401
    )
  end

  def create_params
    params.require(:user).permit(:email, :password, :type)
  end
end