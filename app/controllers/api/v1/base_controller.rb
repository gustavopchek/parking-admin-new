class Api::V1::BaseController < ApplicationController
	protect_from_forgery with: :null_session

  before_action :destroy_session
  before_action :authenticate!
	
	rescue_from ActiveRecord::RecordNotFound, with: :not_found

  def destroy_session
    request.session_options[:skip] = true
  end
	
	def not_found
		return api_error(status: 404, errors: 'Não Encontrado')
	end

	 def authenticate!
    token, options = ActionController::HttpAuthentication::Token.token_and_options(request)

    # render json: options

    if(options.blank?)
      return unauthenticated!
    end

    if(!options.has_key?(:type))
      return unauthenticated!
    end

    type = options[:type]

    # render json: options

    if type == "customer"
      customer_email = options.blank?? nil : options[:email]
      customer = customer_email && Customer.find_by(email: customer_email)

      # render json: customer

      if customer && ActiveSupport::SecurityUtils.secure_compare(customer.authentication_token, token)
        @current_customer = customer
      else
        return unauthenticated!
      end
    elsif type == "agent"

      agent_email = options.blank?? nil : options[:email]
      agent = agent_email && Agent.find_by(email: agent_email)

      if agent && ActiveSupport::SecurityUtils.secure_compare(agent.authentication_token, token)
        @current_agent = agent
      else
        return unauthenticated!
      end
    else
      return unauthenticated!
    end

  end

  #aprimorar
  private
    def api_error(error)
      warden.custom_failure!
      render(
        json: {:errors => ["Erro ao processar solicitação."]},
        status: 401
      )
    end

    def invalid_user
      warden.custom_failure!
      render(
        json: {:errors => ["Usuário não encontrado."]},
        status: 401
      )
    end

    def unauthenticated!
      warden.custom_failure!
      render(
        json: "Acesso Negado.",
        status: 403
      )
    end

    def unauthorized!
      warden.custom_failure!
      render(
        json: "Acesso Negado.",
        status: 403
      )
    end
end