class Api::V1::PurchasesController < Api::V1::BaseController
  include ActiveHashRelation

  def index
    purchases = Purchase.all

    purchases = apply_filters(purchases, params)

    render(
      json: ActiveModel::Serializer::CollectionSerializer.new(
        purchases,
        serializer: Api::V1::PurchaseSerializer,
        root: 'purchases',
      )
    )
  end

  def customer

    return invalid_customer unless @current_customer

    purchases = @current_customer.purchases.order(id: :desc)

    render(
      json: ActiveModel::Serializer::CollectionSerializer.new(
        purchases,
        serializer: Api::V1::PurchaseSerializer,
        root: 'purchases',
      )
    )
  end
  
  def show
    purchase = Purchase.find(params[:id])

    render json: Api::V1::PurchaseSerializer.new(purchase).to_json
  end

  # POST /purchases
  # POST /purchases.json
  def create

    return invalid_customer unless @current_customer

    @purchase = Purchase.new(purchase_params)

    @purchase.customer = @current_customer

    customer = Customer.find(@purchase.customer_id)
      return invalid_customer unless customer

    if(@purchase.value != 10 && @purchase.value != 20 && @purchase.value != 30)
      return invalid_value
    end


    if @purchase.save 
      newcash = customer.cash + @purchase.value

      customer.cash = newcash

      customer.save

      render json: Api::V1::PurchaseSerializer.new(@purchase).to_json, status: 200
    else
      render json: @purchase.errors, status: 422
    end
  end

  private

    def invalid_customer
      warden.custom_failure!
      render(
        json: {:errors => ["Usuário não encontrado."]},
        status: 401
      )
    end

    def invalid_value
      warden.custom_failure!
      render(
        json: {:errors => ["Valor incorreto."]},
        status: 401
      )
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_purchase
      @purchase = Purchase.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def purchase_params
      params.require(:purchase).permit(:value)
    end
end
