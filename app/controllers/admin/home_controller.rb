class Admin::HomeController < ApplicationController
	before_action :authenticate_admin_user!
  def index
  	@activations = []
  	activations = Activation.all.order(id: :desc)
  	activations.each do |activation|
  		if(activation.remaining_minutes > 0)
  			@activations << activation
  		end
  	end
  end
end
