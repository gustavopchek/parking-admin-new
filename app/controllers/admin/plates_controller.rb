class Admin::PlatesController < ApplicationController
  before_action :set_plate, only: [:show, :edit, :update, :destroy]

  # GET /admin/plates
  # GET /admin/plates.json
  def index
    @plates = Plate.all.order(id: :desc)
  end

  # GET /admin/plates/1
  # GET /admin/plates/1.json
  def show
  end

  # GET /admin/plates/new
  def new
    @plate = Plate.new
  end

  # GET /admin/plates/1/edit
  def edit
  end

  # POST /admin/plates
  # POST /admin/plates.json
  def create
    @plate = Plate.new(plate_params)

    respond_to do |format|
      if @plate.save
        format.html { redirect_to [:admin, @plate], notice: 'Plate was successfully created.' }
        format.json { render :show, status: :created, location: @plate }
      else
        format.html { render :new }
        format.json { render json: @plate.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/plates/1
  # PATCH/PUT /admin/plates/1.json
  def update
    respond_to do |format|
      if @plate.update(plate_params)
        format.html { redirect_to [:admin, @plate], notice: 'Plate was successfully updated.' }
        format.json { render :show, status: :ok, location: @plate }
      else
        format.html { render :edit }
        format.json { render json: @plate.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/plates/1
  # DELETE /admin/plates/1.json
  def destroy
    @plate.destroy
    respond_to do |format|
      format.html { redirect_to admin_plates_url, notice: 'Plate was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_plate
      @plate = Plate.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def plate_params
      params.require(:plate).permit(:code)
    end
end
