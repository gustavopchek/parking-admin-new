class Admin::PenaltyPaymentsController < Admin::AdminController
  before_action :set_penalty_payment, only: [:destroy]

  # POST /penalty_payments
  # POST /penalty_payments.json
  def create
    @penalty_payment = PenaltyPayment.new(penalty_payment_params)

    respond_to do |format|
      if @penalty_payment.save
        format.html { redirect_to admin_penalties_url, notice: 'PenaltyPayment was successfully destroyed.' }
        format.json { head :no_content }
      else
        format.html { render :new }
        format.json { render json: @penalty_payment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /penalty_payments/1
  # DELETE /penalty_payments/1.json
  def destroy
    @penalty_payment.destroy
    respond_to do |format|
      format.html { redirect_to admin_penalties_url, notice: 'PenaltyPayment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_penalty_payment
      @penalty_payment = PenaltyPayment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def penalty_payment_params
      params.require(:penalty_payment).permit(:value, :penalty_id, :customer_id)
    end
end
