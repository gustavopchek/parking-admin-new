class Activation < ActiveRecord::Base

	validates :customer_id, presence: true
	validates :ticket_id, presence: true

	belongs_to :ticket
	belongs_to :customer
	belongs_to :plate

	def remaining
		remaining = (((self.created_at - DateTime.now) / 60) + self.ticket.duration).to_i
		if remaining <= 0
			return 'Finalizado'
		else
			return "#{remaining} minutos"
		end
	end

	def percentage
		a = self.ticket.duration
		b = 100
		c = self.remaining_minutes
		d = (b*c)/a

		if d <= 0
			return 0
		else
			return d
		end
	end

	def remaining_minutes
		return (((self.created_at - DateTime.now) / 60) + self.ticket.duration).to_i
	end
end
