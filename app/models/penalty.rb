class Penalty < ActiveRecord::Base
  belongs_to :plate
  belongs_to :agent
  has_one :penalty_payment

  before_create :generate_limit

	def generate_limit
    self.limit = DateTime.now + 7.days
	end
end
