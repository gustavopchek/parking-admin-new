class Plate < ActiveRecord::Base
	validates :code, presence: true, allow_blank: false, length: { is: 8 }
	has_many :activations
	has_many :penalties
	has_and_belongs_to_many :customers, :uniq => true
end
