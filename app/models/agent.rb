class Agent < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :name, presence: true, allow_blank: false

  attr_accessor :current_password

  has_many :penalties

 	before_create :generate_authentication_token

	def generate_authentication_token
	  loop do
	    self.authentication_token = SecureRandom.base64(64)
	    break unless Customer.find_by(authentication_token: authentication_token)
	  end
	end
end
