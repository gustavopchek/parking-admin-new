class PenaltyPayment < ActiveRecord::Base
	belongs_to :penalty
	belongs_to :customer

	validates :penalty, uniqueness: true

end
