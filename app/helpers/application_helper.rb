module ApplicationHelper
	def format_date(date)
		date.strftime("%d/%m/%Y")
	end

	def page_title(page_title)
		page_title = "Uma nova forma de Estacionar" unless page_title.size > 0
	  "WayPark - #{page_title}"
	end

	def clock_color(minutes)
		case minutes
		when 16..500
			return 'green'
		when 11..15
			return 'yellow'
		when 6..10
			return 'orange'
		else
			return 'red'
		end
	end
end
