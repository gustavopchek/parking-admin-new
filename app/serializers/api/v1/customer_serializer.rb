class Api::V1::CustomerSerializer < Api::V1::BaseSerializer
  attributes :id, :email, :name, :phone, :created_at, :updated_at, :cash, :token

  # has_many :activations
  # has_many :plates

  def cash
  	ActionController::Base.helpers.number_to_currency(object.cash, unit: "R$ ", separator: ",", delimiter: "")
  end

  def created_at
    object.created_at.in_time_zone.iso8601 if object.created_at
  end

  def updated_at
    object.updated_at.in_time_zone.iso8601 if object.created_at
  end

  def token
    object.authentication_token
  end
end
