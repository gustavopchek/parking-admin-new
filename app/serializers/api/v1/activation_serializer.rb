class Api::V1::ActivationSerializer < Api::V1::BaseSerializer
  attributes :id, :customer_id, :ticket_id, :plate_id, :remaining, :created_at

  belongs_to :ticket
  belongs_to :customer
  belongs_to :plate

  def created_at
    object.created_at.in_time_zone.iso8601 if object.created_at
  end

  def updated_at
    object.updated_at.in_time_zone.iso8601 if object.created_at
  end

  def remaining
  	object.remaining_minutes
  end

end
