class Api::V1::PurchaseSerializer < Api::V1::BaseSerializer
  attributes :id, :value, :customer_id, :created_at

  # belongs_to :customer

  def value
  	ActionController::Base.helpers.number_to_currency(object.value, unit: "R$ ", separator: ",", delimiter: "")
  end

  def created_at
    object.created_at.in_time_zone.iso8601 if object.created_at
  end

  def updated_at
    object.updated_at.in_time_zone.iso8601 if object.created_at
  end

end
