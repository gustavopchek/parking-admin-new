class Api::V1::TicketSerializer < Api::V1::BaseSerializer
  attributes :id, :name, :duration, :price

  def price
  	ActionController::Base.helpers.number_to_currency(object.price, unit: "R$ ", separator: ",", delimiter: "")
  end

  def created_at
    object.created_at.in_time_zone.iso8601 if object.created_at
  end

  def updated_at
    object.updated_at.in_time_zone.iso8601 if object.created_at
  end
end
