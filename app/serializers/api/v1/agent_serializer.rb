class Api::V1::AgentSerializer < Api::V1::BaseSerializer
  attributes :id, :email, :name, :created_at, :updated_at, :token

  def created_at
    object.created_at.in_time_zone.iso8601 if object.created_at
  end

  def updated_at
    object.updated_at.in_time_zone.iso8601 if object.created_at
  end

  def token
    object.authentication_token
  end
end
