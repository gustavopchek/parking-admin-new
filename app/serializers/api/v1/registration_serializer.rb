class Api::V1::RegistrationSerializer < Api::V1::BaseSerializer
  #just some basic attributes
  attributes :id, :name, :email, :phone, :cash, :token

  def cash
  	ActionController::Base.helpers.number_to_currency(object.cash, unit: "R$ ", separator: ",", delimiter: "")
  end

  def token
    object.authentication_token
  end
end