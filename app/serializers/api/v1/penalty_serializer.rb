class Api::V1::PenaltySerializer < Api::V1::BaseSerializer
  attributes :id, :description, :value, :plate_id, :created_at, :limit, :expired

  belongs_to :plate
  # belongs_to :agent
  has_one :penalty_payment

  def value
    ActionController::Base.helpers.number_to_currency(object.value, unit: "R$ ", separator: ",", delimiter: "")
  end

  def created_at
    object.created_at.in_time_zone.iso8601 if object.created_at
  end

  def updated_at
    object.updated_at.in_time_zone.iso8601 if object.created_at
  end

  def limit
    object.limit.in_time_zone.iso8601 if object.limit
  end

  def expired
    object.limit < DateTime.now ? true : false
  end

  def remaining
  	object.remaining_minutes
  end

end
