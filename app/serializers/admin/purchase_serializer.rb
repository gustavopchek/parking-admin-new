class Admin::PurchaseSerializer < ActiveModel::Serializer
  attributes :id, :value
  has_one :customer
end
