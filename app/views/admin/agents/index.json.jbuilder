json.array!(@agents) do |admin_agent|
  json.extract! admin_agent, :id
  json.url admin_agent_url(admin_agent, format: :json)
end
