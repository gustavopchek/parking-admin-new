json.array!(@admin_purchases) do |admin_purchase|
  json.extract! admin_purchase, :id, :value, :customer_id
  json.url admin_purchase_url(admin_purchase, format: :json)
end
