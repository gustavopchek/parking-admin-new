json.array!(@penalties) do |penalty|
  json.extract! penalty, :id, :description, :value, :plate_id
  json.url penalty_url(penalty, format: :json)
end
