json.array!(@customers) do |admin_customer|
  json.extract! admin_customer, :id
  json.url admin_customer_url(admin_customer, format: :json)
end
