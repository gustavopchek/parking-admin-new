json.array!(@activations) do |activation|
  json.extract! activation, :id, :ticket_id, :customer_id
  json.url activation_url(activation, format: :json)
end
