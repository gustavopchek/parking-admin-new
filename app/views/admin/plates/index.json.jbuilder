json.array!(@plates) do |plate|
  json.extract! plate, :id
  json.url admin_plate_url(plate, format: :json)
end
