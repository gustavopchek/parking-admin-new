require 'test_helper'

class Admin::ActivationsControllerTest < ActionController::TestCase
  setup do
    @activation = activations(:Activation1)
    sign_in users(:User1)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:activations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create activation" do
    assert_difference('Activation.count') do
      post :create, activation: { customer_id: @activation.customer_id, ticket_id: @activation.ticket_id }
    end

    assert_redirected_to admin_activation_path(assigns(:activation))
  end

  test "should show activation" do
    get :show, id: @activation
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @activation
    assert_response :success
  end

  test "should update activation" do
    patch :update, id: @activation, activation: { customer_id: @activation.customer_id, ticket_id: @activation.ticket_id }
    assert_redirected_to admin_activation_path(assigns(:activation))
  end

  test "should destroy activation" do
    assert_difference('Activation.count', -1) do
      delete :destroy, id: @activation
    end

    assert_redirected_to admin_activations_path
  end
end
