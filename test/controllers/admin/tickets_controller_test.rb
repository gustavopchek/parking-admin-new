require 'test_helper'

class Admin::TicketsControllerTest < ActionController::TestCase
  setup do
    @ticket = tickets(:Ticket1)
    sign_in users(:User1)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tickets)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ticket" do
    assert_difference('Ticket.count') do
      post :create, ticket: { name: 'Test Ticket', description: 'Test Ticket description', duration: 30, price: 0.5 }
    end

    assert_redirected_to admin_ticket_path(assigns(:ticket))
  end

  test "should show ticket" do
    get :show, id: @ticket
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @ticket
    assert_response :success
  end

  test "should update ticket" do
    patch :update, id: @ticket, ticket: { name: 'Test Ticket Updated' }
    assert_redirected_to admin_ticket_path(assigns(:ticket))
  end

  test "should destroy ticket" do
    assert_difference('Ticket.count', -1) do
      delete :destroy, id: @ticket
    end

    assert_redirected_to admin_tickets_path
  end
end
