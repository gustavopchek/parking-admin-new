require 'test_helper'

class Admin::PlatesControllerTest < ActionController::TestCase
  setup do
    @admin_plate = admin_plates(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_plates)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admin_plate" do
    assert_difference('Admin::Plate.count') do
      post :create, admin_plate: {  }
    end

    assert_redirected_to admin_plate_path(assigns(:admin_plate))
  end

  test "should show admin_plate" do
    get :show, id: @admin_plate
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @admin_plate
    assert_response :success
  end

  test "should update admin_plate" do
    patch :update, id: @admin_plate, admin_plate: {  }
    assert_redirected_to admin_plate_path(assigns(:admin_plate))
  end

  test "should destroy admin_plate" do
    assert_difference('Admin::Plate.count', -1) do
      delete :destroy, id: @admin_plate
    end

    assert_redirected_to admin_plates_path
  end
end
