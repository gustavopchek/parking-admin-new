require 'test_helper'

class Admin::CustomersControllerTest < ActionController::TestCase
  setup do
    @customer = customers(:Customer1)
    sign_in users(:User1)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:customers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create customer" do
    assert_difference('Customer.count') do
      post :create, customer: { email: 'teste123@teste321.com', password: '123456' }
    end

    assert_redirected_to admin_customer_path(assigns(:customer))
  end

  test "should show customer" do
    get :show, id: @customer
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @customer
    assert_response :success
  end

  # test "should update customer" do
  #   patch :update, id: @customer, customer: { email: 'email123@email321.com' }
  #   assert_redirected_to admin_customer_path(assigns(:customer))
  # end

  test "should destroy customer" do
    assert_difference('Customer.count', -1) do
      delete :destroy, id: @customer
    end

    assert_redirected_to admin_customers_path
  end
end
