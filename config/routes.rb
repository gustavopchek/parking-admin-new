Rails.application.routes.draw do
  
  # devise_for :agents
  # devise_for :customers
  # devise_for :users
  get 'home/index'

  namespace :admin do
    get '', to: 'home#index', as: '/' 

    resources :tickets
    resources :customers
    resources :activations
    resources :plates
    resources :penalties do
      get '/payment' => 'penalties#payment', as: :pay
    end
    resources :purchases
    resources :agents
    resources :penalty_payments, only: [:create, :destroy]

    authenticated :user do
      devise_scope :user do
        root to: 'admin/home#index', as: :root
      end
    end

    unauthenticated do
      devise_scope :user do
        root to: 'admin/sessions#new', as: :unauthenticated_root
        #root to: 'home#index', as: :root
      end
    end

    devise_for :users, controllers: {
      sessions: 'admin/sessions',
      registrations: 'admin/registrations',
      passwords: 'admin/passwords'
    }
  end

  namespace :api do
    namespace :v1 do
      resources :customers, only: [:index, :create, :show, :update, :destroy]
      resources :agents, only: [:index, :create, :show, :update, :destroy]
      resources :tickets, only: [:index, :create, :show, :update, :destroy]
      resources :activations, only: [:index, :create, :show, :update, :destroy] do
        collection do
          get 'last_activation' => 'activations#last_activation', as: :last_activation
          get ':plate_code/last_activations' => 'activations#last_activations', as: :last_activations
          get ':customer_id' => 'activations#customer', as: :customer
        end
      end
      resources :penalties, only: [:index, :create, :show, :update, :destroy] do
        collection do
          get '/plate/:plate_id' => 'penalties#plate', as: :plate
          get '/customer/' => 'penalties#customer', as: :customer
          get '/agent/' => 'penalties#agent', as: :agent
          post '/payment' => 'penalties#payment', as: :payment
        end
      end
      resources :purchases, only: [:index, :create, :show, :update, :destroy] do
        collection do
          get '/customer/' => 'purchases#customer', as: :customer
        end
      end
      
      resources :plates, only: [:index, :create, :show, :update, :destroy] do
        collection do
          get '/customer/' => 'plates#customer', as: :customer
        end
      end

      devise_scope :customer do
        resources :sessions, only: [:create]
        resources :registrations, only: [:create, :update]
      end

      devise_scope :agents do
        resources :sessions, only: [:create]
        resources :registrations, only: [:create, :update]
      end
    end
  end

  # devise_for :users, path: 'admin', controllers: {
  #   sessions: 'user/sessions',
  #   registrations: 'user/registrations',
  #   passwords: 'user/passwords'
  # }
  
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
