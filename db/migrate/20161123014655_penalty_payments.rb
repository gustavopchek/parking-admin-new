class PenaltyPayments < ActiveRecord::Migration
  def change
  	create_table :penalty_payments do |t|
	  	t.string :description
	    t.decimal :value, precision: 10, scale: 2
	    t.references :penalty, index: true, foreign_key: true, unique: true
	    t.references :customer, index: true, foreign_key: true

	    t.timestamps null: false
	  end
  end
end
