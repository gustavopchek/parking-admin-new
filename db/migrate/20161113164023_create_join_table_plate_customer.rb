class CreateJoinTablePlateCustomer < ActiveRecord::Migration
  def change
    create_join_table :plates, :customers do |t|
      t.index [:plate_id, :customer_id], :unique => true
      t.index [:customer_id, :plate_id]
    end
  end
end
