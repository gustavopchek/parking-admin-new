class CreateActivations < ActiveRecord::Migration
  def change
    create_table :activations do |t|

      t.references :ticket, foreign_key: true, null: false
      t.references :customer, index: true, foreign_key: true, null: false

      t.timestamps null: false
    end

  end
end
