class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.string :name, required: true
	  t.string :description
	  t.integer :duration
	  t.decimal :price, precision: 10, scale: 2
	  t.integer :status, default: 0

      t.timestamps null: false
    end
  end
end