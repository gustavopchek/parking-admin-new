class CreatePlates < ActiveRecord::Migration
  def change
    create_table :plates do |t|

    	t.string :code

      t.timestamps null: false
    end

    add_index :plates, :code, unique: true
  end
end
