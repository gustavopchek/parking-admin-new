class AddAgentRefToPenalties < ActiveRecord::Migration
  def change
  	add_reference :penalties, :agent, index: true, foreign_key: true
  end
end

