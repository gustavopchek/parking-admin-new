class AddDefaultToCustomerCash < ActiveRecord::Migration
  def change
  	change_column :customers, :cash, :decimal, precision: 10, scale: 2, null: false, default: 0.0
  end
end
