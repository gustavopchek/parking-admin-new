class AddPlateRefToActivations < ActiveRecord::Migration
  def change
  	add_reference :activations, :plate, index: true, foreign_key: true
  end
end
