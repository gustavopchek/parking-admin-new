class CreatePenalties < ActiveRecord::Migration
  def change
    create_table :penalties do |t|
      t.string :description
      t.decimal :value, precision: 10, scale: 2
      t.references :plate, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
