class CreatePurchases < ActiveRecord::Migration
  def change
    create_table :purchases do |t|
      t.decimal :value, precision: 10, scale: 2
      t.references :customer, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
