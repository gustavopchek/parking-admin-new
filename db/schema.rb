# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161129024141) do

  create_table "activations", force: :cascade do |t|
    t.integer  "ticket_id",   null: false
    t.integer  "customer_id", null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "plate_id"
  end

  add_index "activations", ["customer_id"], name: "index_activations_on_customer_id"
  add_index "activations", ["plate_id"], name: "index_activations_on_plate_id"

  create_table "agents", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "authentication_token",                null: false
    t.string   "name"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "agents", ["email"], name: "index_agents_on_email", unique: true
  add_index "agents", ["reset_password_token"], name: "index_agents_on_reset_password_token", unique: true

  create_table "customers", force: :cascade do |t|
    t.string   "email",                                           default: "",  null: false
    t.string   "encrypted_password",                              default: "",  null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                                   default: 0,   null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "authentication_token",                                          null: false
    t.string   "name"
    t.string   "cpf"
    t.string   "phone"
    t.decimal  "cash",                   precision: 10, scale: 2, default: 0.0, null: false
    t.datetime "created_at",                                                    null: false
    t.datetime "updated_at",                                                    null: false
  end

  add_index "customers", ["authentication_token"], name: "index_customers_on_authentication_token", unique: true
  add_index "customers", ["email"], name: "index_customers_on_email", unique: true
  add_index "customers", ["reset_password_token"], name: "index_customers_on_reset_password_token", unique: true

  create_table "customers_plates", id: false, force: :cascade do |t|
    t.integer "plate_id",    null: false
    t.integer "customer_id", null: false
  end

  add_index "customers_plates", ["customer_id", "plate_id"], name: "index_customers_plates_on_customer_id_and_plate_id"
  add_index "customers_plates", ["plate_id", "customer_id"], name: "index_customers_plates_on_plate_id_and_customer_id", unique: true

  create_table "penalties", force: :cascade do |t|
    t.string   "description"
    t.decimal  "value",       precision: 10, scale: 2
    t.integer  "plate_id"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "agent_id"
    t.datetime "limit"
  end

  add_index "penalties", ["agent_id"], name: "index_penalties_on_agent_id"
  add_index "penalties", ["plate_id"], name: "index_penalties_on_plate_id"

  create_table "penalty_payments", force: :cascade do |t|
    t.string   "description"
    t.decimal  "value",       precision: 10, scale: 2
    t.integer  "penalty_id"
    t.integer  "customer_id"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  add_index "penalty_payments", ["customer_id"], name: "index_penalty_payments_on_customer_id"
  add_index "penalty_payments", ["penalty_id"], name: "index_penalty_payments_on_penalty_id"

  create_table "plates", force: :cascade do |t|
    t.string   "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "plates", ["code"], name: "index_plates_on_code", unique: true

  create_table "purchases", force: :cascade do |t|
    t.decimal  "value",       precision: 10, scale: 2
    t.integer  "customer_id"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  add_index "purchases", ["customer_id"], name: "index_purchases_on_customer_id"

  create_table "tickets", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "duration"
    t.decimal  "price",       precision: 10, scale: 2
    t.integer  "status",                               default: 0
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "name"
    t.string   "username"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
